package simple.insert;

import my.bak.trafic.core.plugin.transport.ImportDataBuilder;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

public class Parser {

    private static final String REGEX = "\\|";

    private SimpleDateFormat formatter;

    public Parser() {
        this.formatter = new SimpleDateFormat("EEE MMM dd HH:mm:ss yyyy");
    }

    /**
     * Parse one line
     * @param line
     * @return return filled structure for host aplication
     */
    public ImportDataBuilder parseLine(String line) throws ParseException {
        ImportDataBuilder builder = new ImportDataBuilder();
        String[] splited = line.split(REGEX);
        if(splited.length != 7){
            throw new ParseException("Can not parse line: " + line,-1);
        }
        builder.setDirection(splited[1]);
        builder.setPlace(splited[0]);
        builder.setBeginDate(formatter.parse(splited[2]));
        builder.setEndDate(formatter.parse(splited[3]));

        Map<String,String> values = new HashMap<>();
        values.put("Rychlost",splited[4]);
        values.put("Obsazenost",splited[5]);
        values.put("Typ vozidla",splited[6]);

        builder.setData(values);

        return builder;
    }
}
