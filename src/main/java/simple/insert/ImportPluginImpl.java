package simple.insert;

import com.google.inject.Injector;
import javafx.scene.control.Alert;
import my.bak.trafic.core.event.Bus;
import my.bak.trafic.core.event.types.AlertEvent;
import my.bak.trafic.core.plugin.ImportPlugin;
import my.bak.trafic.core.plugin.exception.WrongParametersException;
import my.bak.trafic.core.plugin.transport.ImportDataBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.text.ParseException;

public class ImportPluginImpl implements ImportPlugin {

    private Logger logger;
    private File imputFile;
    private BufferedReader reader;
    private boolean hashNext = true;
    private Bus bus;
    private Parser parser;
    private String line;


    @Override
    public String getName() {
        return "Simple Import plugin";
    }

    @Override
    public String getDescription() {
        return "Import data from file";
    }

    @Override
    public void setUtility(Injector injector) {
        bus = injector.getInstance(Bus.class);
    }

    @Override
    public void init(Logger logger) {
        this.logger = logger;
        logger.info("Initialize plugin");
        parser = new Parser();
    }

    @Override
    public void setParameters(String parameters) throws WrongParametersException {
        logger.info("Recieve parameters: {}", parameters);
        parameters = parameters.trim();
        imputFile = new File(parameters);

        if (!Files.isReadable(imputFile.toPath())) {
            throw new WrongParametersException("File not exist");
        }
        if (imputFile.isDirectory()) {
            throw new WrongParametersException("Is not file");
        }
    }

    @Override
    public void start() {
        line = null;
        try {
            reader = Files.newBufferedReader(imputFile.toPath());
            hashNext = (line = reader.readLine()) != null;
        } catch (IOException e) {
            logger.fatal(e);
            hashNext = false;
            bus.publishEvent(new AlertEvent(Alert.AlertType.WARNING, "Import plugin error", "Cannot open file: " + imputFile.getAbsolutePath(), e));
        }
    }


    @Override
    public void finish() {
        try {
            reader.close();
        } catch (IOException e) {
            logger.fatal(e);
            bus.publishEvent(new AlertEvent(Alert.AlertType.WARNING, "Plugin import", "Cannot close buffered reader", e));
        }
    }

    @Override
    public void dispose() {
        bus = null;
        reader = null;
        logger.info("Plugin was disposed");
    }

    @Override
    public ImportDataBuilder importData() {
        try {
            ImportDataBuilder data = parser.parseLine(line);
            hashNext = (line = reader.readLine()) != null;
            return data;
        } catch (ParseException | IOException e) {
            hashNext = false;
            logger.fatal(e);
            bus.publishEvent(new AlertEvent(Alert.AlertType.WARNING, "Plugin import", "Cannot parse line", e));
        }
        return null;
    }

    @Override
    public boolean hasNext() {
        return hashNext;
    }

}
